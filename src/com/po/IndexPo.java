package com.po;

import java.util.List;
import java.util.Map;

public class IndexPo {
    public IndexPo(List<GoodsType> goodsTypes, List<Map<String, Object>> saleorders, List<Map<String, Object>> focusorders, List<Map<String, Object>> notices, List<Map<String, Object>> lastedGoods) {
        this.goodsTypes = goodsTypes;
        this.saleorders = saleorders;
        this.focusorders = focusorders;
        this.notices = notices;
        this.lastedGoods = lastedGoods;
    }

    private List<GoodsType> goodsTypes;

    private List<Map<String, Object>> saleorders;

    private List<Map<String, Object>> focusorders;

    private List<Map<String, Object>> notices;

    private List<Map<String, Object>> lastedGoods;

    public List<GoodsType> getGoodsTypes() {
        return goodsTypes;
    }

    public void setGoodsTypes(List<GoodsType> goodsTypes) {
        this.goodsTypes = goodsTypes;
    }

    public List<Map<String, Object>> getSaleorders() {
        return saleorders;
    }

    public void setSaleorders(List<Map<String, Object>> saleorders) {
        this.saleorders = saleorders;
    }

    public List<Map<String, Object>> getFocusorders() {
        return focusorders;
    }

    public void setFocusorders(List<Map<String, Object>> focusorders) {
        this.focusorders = focusorders;
    }

    public List<Map<String, Object>> getNotices() {
        return notices;
    }

    public void setNotices(List<Map<String, Object>> notices) {
        this.notices = notices;
    }

    public List<Map<String, Object>> getLastedGoods() {
        return lastedGoods;
    }

    public void setLastedGoods(List<Map<String, Object>> lastedGoods) {
        this.lastedGoods = lastedGoods;
    }
}
