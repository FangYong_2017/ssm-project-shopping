package com.controller.before;
import javax.servlet.http.HttpSession;

import com.exception.MyExceptionHandler;
import com.po.IndexPo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.po.Goods;
import com.service.before.IndexService;
@Controller
public class IndexController {

	private final static Logger logger = LoggerFactory.getLogger(IndexController.class);

	@Autowired
	private IndexService indexService;
	/**
	 * 首页
	 */
	@RequestMapping("/before")
	public String before(Model model,HttpSession session, Goods goods) {
		logger.info("before的访问开始：---------------------");
		IndexPo indexPo=indexService.before(goods);
		if (indexPo==null||indexPo.getGoodsTypes()==null||indexPo.getFocusorders()==null||indexPo.getNotices()==null||indexPo.getLastedGoods()==null){
			logger.info("before的访问：从后台获取的数据为空");
		}
		session.setAttribute("goodsType", indexPo.getGoodsTypes());
		model.addAttribute("salelist", indexPo.getSaleorders());
		model.addAttribute("focuslist", indexPo.getFocusorders());
		model.addAttribute("noticelist", indexPo.getNotices());
		model.addAttribute("lastedlist", indexPo.getLastedGoods());
		logger.info("before的访问结束：---------------------");
		return "before/index";
	}

	@RequestMapping("/index")
	public String index(Model model,HttpSession session, Goods goods) {
		return before(model,session,goods);
	}
	/**
	 * 转到注册页面
	 */
	@RequestMapping("/toRegister")
	public String toRegister(Model model) {
		return indexService.toRegister(model);
	}
	/**
	 * 转到登录页面
	 */
	@RequestMapping("/toLogin")
	public String toLogin(Model model) {
		return indexService.toLogin(model);
	}
	/**
	 * 转到商品详情页
	 */
	@RequestMapping("/goodsDetail")
	public String goodsDetail(Model model,Integer id) {
		return indexService.goodsDetail(model, id);
	}
	/**
	 * 转到公告页面
	 */
	@RequestMapping("/selectANotice")
	public String selectANotice(Model model,Integer id) {
		return indexService.selectANotice(model, id);
	}
	/**
	 * 首页搜索
	 */
	@RequestMapping("/search")
	public String search(Model model,String mykey) {
		return indexService.search(model, mykey);
	}
}
